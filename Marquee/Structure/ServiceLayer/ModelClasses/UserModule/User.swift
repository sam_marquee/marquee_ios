//
//  Body.swift
//
//  Created by Abdul Muqeem on 25/12/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSObject , NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let city = "city"
        static let email = "email"
        static let id = "id"
        static let fullName = "full_name"
        static let updatedAt = "updated_at"
        static let createdAt = "created_at"
        static let accessToken = "access_token"
        static let phoneNumber = "phone_number"
    }
    
    // MARK: Properties
    public var city: String?
    public var email: String?
    public var id: Int?
    public var fullName: String?
    public var updatedAt: String?
    public var createdAt: String?
    public var accessToken: String?
    public var phoneNumber: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        city = json[SerializationKeys.city].string
        email = json[SerializationKeys.email].string
        id = json[SerializationKeys.id].int
        fullName = json[SerializationKeys.fullName].string
        updatedAt = json[SerializationKeys.updatedAt].string
        createdAt = json[SerializationKeys.createdAt].string
        accessToken = json[SerializationKeys.accessToken].string
        phoneNumber = json[SerializationKeys.phoneNumber].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = city { dictionary[SerializationKeys.city] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = accessToken { dictionary[SerializationKeys.accessToken] = value }
        if let value = phoneNumber { dictionary[SerializationKeys.phoneNumber] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.fullName = aDecoder.decodeObject(forKey: SerializationKeys.fullName) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.accessToken = aDecoder.decodeObject(forKey: SerializationKeys.accessToken) as? String
        self.phoneNumber = aDecoder.decodeObject(forKey: SerializationKeys.phoneNumber) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(city, forKey: SerializationKeys.city)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(fullName, forKey: SerializationKeys.fullName)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(accessToken, forKey: SerializationKeys.accessToken)
        aCoder.encode(phoneNumber, forKey: SerializationKeys.phoneNumber)
    }
    
}
