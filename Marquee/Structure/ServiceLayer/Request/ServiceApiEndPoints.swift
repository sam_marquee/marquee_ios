//
//  ServiceApiEndPoints.swift
//  TAAJ
//
//  Created by Abdul Muqeem on 22/03/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation

class ServiceApiEndPoints: NSObject  {

    static let baseURL = "http://marriage-hall-booking.herokuapp.com/"
    
    // User Module
    static let login = baseURL + "users/sign_in.json"
    static let register = baseURL + "users.json"
    static let city = baseURL + "cities.json"
    static let forgotPassword = baseURL + "users/password.json"

    
}
