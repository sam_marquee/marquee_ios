//
//  ImageExtension.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIView {

    /**
     Fade in a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeIn(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    /**
     Fade out a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeOut(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
}


extension UIImageView {

    func setImageFromUrl(urlStr:String) {

        let tempStr = urlStr.replacingOccurrences(of: "\\", with: "/")
        let tempStr1 = tempStr.replacingOccurrences(of: " ", with: "%20")
        let url = URL(string: tempStr1)!
        print(url)
        
        var kingf = self.kf
        kingf.indicatorType = .activity
        kingf.setImage(with: url)
        
    }

}

extension UIButton {
    
    func setImageURL(urlStr:String) {
        
        let url = URL(string: urlStr)!
        print(url)
        self.kf.setImage(with: url, for: .normal)

    }
}

extension UIFont {
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor
            .withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
}

extension UIImage {
    
    func resizedImage(newSize: CGSize) -> UIImage {
        guard self.size != newSize else { return self }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    // when call 
    //image.resizedImage(newSize: CGSize(width: 80, height: 80))
}
