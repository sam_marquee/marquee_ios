//
//  AppDelegate.swift
//  Marquee
//
//  Created by Abdul Muqeem on 16/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        self.NavigateTOInitialViewController()
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {

    }

    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func NavigateTOInitialViewController() {
                
       if UserManager.isUserLogin() {
            
            let nav = RootViewController.instantiateFromStoryboard()
            self.window?.rootViewController = nav
            let vc = HomeViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)
        
        } else {
    
            let check = UserDefaults.standard.object(forKey: "isFirstLaunched") as? String
        
            if check == "true" {
                let nav = RootViewController.instantiateFromStoryboard()
                self.window?.rootViewController = nav
                let vc = LoginViewController.instantiateFromStoryboard()
                nav.pushViewController(vc, animated: true)
            }
            else {
                let nav = RootViewController.instantiateFromStoryboard()
                self.window?.rootViewController = nav
                let vc = TutorialViewController.instantiateFromStoryboard()
                nav.pushViewController(vc, animated: true)
            }
        }
    }

}

