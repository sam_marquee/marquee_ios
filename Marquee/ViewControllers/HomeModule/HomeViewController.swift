//
//  HomeViewController.swift
//  Marquee
//
//  Created by Abdul Muqeem on 17/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
