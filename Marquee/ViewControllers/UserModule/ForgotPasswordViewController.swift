//
//  ForgotPasswordViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 31/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ForgotPasswordViewController: AlertViewDelegate {
    
    func okAction() {
        if self.textAlert == "password" {
            self.navigationController?.popViewController(animated: true)
        }
        self.alertView.isHidden = true
    }
    
}

class ForgotPasswordViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ForgotPasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ForgotPasswordViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtEmail:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func rememberPasswordAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SignupAction(_ sender : UIButton) {
        let vc = SignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func sendAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["email":email]
        let dataDic:[String:Any] = ["user":params]
        print(dataDic)
        
        UserServices.ForgotPassword(param: dataDic , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let msg = response?["message"].stringValue
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: msg! , id: 0)
            self.alertView.isHidden = false
            self.textAlert = "password"
            
        })        
    }
}
