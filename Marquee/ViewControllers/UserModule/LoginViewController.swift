//
//  LoginViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 30/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension LoginViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class LoginViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        self.LightStatusBar()
        self.navigationController?.HideNavigationBar()
        
    }
    
    @IBAction func ForgotPasswordAction(_ sender : UIButton) {
        let vc = ForgotPasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func SignupAction(_ sender : UIButton) {
        let vc = SignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
        
    @IBAction func facebookAction(_ sender : UIButton) {
        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "will be implemented" , id: 0)
        self.alertView.isHidden = false
        return
    }
    
    @IBAction func googleAction(_ sender : UIButton) {
        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "will be implemented" , id: 0)
        self.alertView.isHidden = false
        return
    }
    
    @IBAction func loginAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let password = self.txtPassword.text!
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , style: .warning)
            return
        }
        
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["email":email , "password":password]
        let dataDic:[String:Any] = ["user":params]
        print(dataDic)
        
        UserServices.Login(param: dataDic, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
     
            let userResultObj = User(object:(response?["body"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            let vc = HomeViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
    }
    
}
