//
//  SignUpViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 31/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown

extension SignUpViewController: AlertViewDelegate {
    
    func okAction() {
        
        if self.textAlert == "register" {
            
            let vc = HomeViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.alertView.isHidden = true
    }
    
}

class SignUpViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SignUpViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignUpViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtNumber:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var cityView:UIView!
    
    var type:String = ""
    
    var cityDropDown = DropDown()
    var cityArray:[CityList]? = [CityList]()
    var cityId:Int! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.GetCityList()
        
        // City DropDown
        self.cityDropDown.anchorView = self.cityView
        self.cityDropDown.bottomOffset = CGPoint(x: 0 , y: (self.cityDropDown.anchorView?.plainView.bounds.height)!)
        self.cityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in

            self.txtCity.text = item
            
            for a in self.cityArray! {
                
                if item == a.name {
                    self.cityId = a.id!
                    print("City Name \(item) & Id is \(self.cityId!)")
                }
            }

        }
        
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func cityAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.cityDropDown.show()
    }
    
    //MARK:-  City Listing Service Calling
    
    func GetCityList() {
        
        self.view.endEditing(true)
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        UserServices.CityList(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            if let ResponseArray = response?["body"].array {
                
                var cityItem = [String]()
                
                for resultObj in ResponseArray {
                    let obj =  CityList(json: resultObj)
                    self.cityArray?.append(obj)
                    if let title = obj.name {
                        cityItem.append((title))
                    }
                }
                
                self.cityDropDown.dataSource = cityItem
                
            }
            
        })
        
    }
    
    @IBAction func signupAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let name = self.txtName.text!
        let password = self.txtPassword.text!
        let city = self.txtCity.text!
        let number = self.txtNumber.text!
        
        if name.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter full name" , style: .warning)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        if city.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please select city" , style: .warning)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , style: .warning)
            return
        }
        
        if number.count != 0 {
            if number.count != 11 {
                self.showBanner(title: "Alert", subTitle: "Please enter valid 11 digits number" , style: .warning)
                return
            }
        }
        
        if  password.count < 6  {
            self.showBanner(title: "Alert", subTitle: "Please enter minimum 6 digit password" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["full_name":name , "email" : email , "city": city , "phone_number" : number , "password":password ]
        let dataDic:[String:Any] = ["user":params]
        print(dataDic)
        
        UserServices.Register(param: dataDic, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let userResultObj = User(object:(response?["body"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Congratulations!", msg: "Your Account has been created successfully" , id: 0)
            self.alertView.isHidden = false
            self.textAlert = "register"
            
        })
        
    }
}


